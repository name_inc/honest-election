const choo = require('choo')
const html = require('choo/html')
const d3 = require('d3-fetch')
// const devtools = require('choo-devtools')
// cached component 
const InfoView = require('./info.js')
// todo minify, precache html, dist build folder
const app = choo()
// app.use(devtools())
app.use(stStore)
app.route('/', template())
app.route('/:st', template(st))
app.route('/404', template(notFound))
app.mount('body')

// router state
let prevRoute = null

// store
// us state = st or sts (plural), state = app state
function stStore (state, emitter) {
  // todo cache
  state.selectedBody = 'senate'
  state.sts = {
    az: {house: [], senate: [], executive: []},
    ca: {house: [], senate: [], executive: []},
    co: {house: [], senate: [], executive: []},
    ga: {house: [], senate: [], executive: []},
    ia: {house: [], senate: [], executive: []},
    mi: {house: [], senate: [], executive: []},
    mn: {house: [], senate: [], executive: []},
    mo: {house: [], senate: [], executive: []},
    nh: {house: [], senate: [], executive: []},
    nm: {house: [], senate: [], executive: []},
    nv: {house: [], senate: [], executive: []},
    pa: {house: [], senate: [], executive: []},
    ut: {house: [], senate: [], executive: []},
    va: {house: [], senate: [], executive: []},
    wa: {house: [], senate: [], executive: []},
    wi: {house: [], senate: [], executive: []},
    wy: {house: [], senate: [], executive: []}
  }

  emitter.on('fetch-st', (st) => {
    d3.csv(`./states/${st}.csv`)
      .then((data) => {
        data.filter((item) => {
          if (
            item.Body === "house" || 
            item.Body === "House" || 
            item.Body === "State House" || 
            item.Body === "Representative" || 
            item.Body === "Assembly") 
          {
            state.sts[st].house.push(item)
          } else if (item.Body === "Executive") { 
            state.sts[st].executive.push(item)
          }else {
            state.sts[st].senate.push(item)
          }
          emitter.emit('render')
        })
    })
  })

  emitter.on('change-body', (nextSelectedBody) => {
    state.selectedBody = nextSelectedBody
    emitter.emit('render')
  })
}

function template (childView) {  
  return (state, emit) => {                  
    return html`
      <body>
        ${header(state)}
        ${!childView ? '' : childView(state, emit)}
        ${state.cache(InfoView, 'info').render()}
      </body>
    `
  }
}

function info (state) { return state.cache(InfoView, 'info').render() }

function header(state) {
  return html`
    <header>
      <a class="title no-decoration" href="/">
        <span class="border-bottom">🇺🇸</span> Honest Election 2020</a>
      <div class="states">
      ${
        Object.keys(state.sts)
          .map(st => html`<a class="${isActive(st)} btn" href="/${st}">${st}</a>`)
      }
      </div>
    </header>
  `
  function isActive(st) { return state.params.st === st ? 'active' : '' }
}

// state table view with senate, house buttons
function st (state, emit) {
  const {st} = state.params
  if(prevRoute !== state.href) {
    prevRoute = state.href
    emit('fetch-st', st)
  }
  // todo logic for correct wording: house/assembly
  // todo fix flicker (min-height1000px)
  return html`
  <div style="min-height:1000px">
    <div class="state body-btn-container">
      <div class="state body btn ${isActive('senate')}" onclick=${changeBody}>Senate</div>
      <div class="state body btn ${isActive('house')}" onclick=${changeBody}>House</div>
      ${ state.sts[st].executive.length > 0 ? html`<div class="state body btn ${isActive('executive')}" onclick=${changeBody}>Executive</div>` : `` }
    </div>
      <table id="table">
        ${
          state.sts[st][state.selectedBody]
            .map(legislator => {
              return html`        
                <tr>
                  <td data-label="Name">${legislator["Name"]}</td>
                  <td data-label="District" class="small">${legislator["District"]}</td>
                  <td data-label="Party" class="small">${legislator["Party"]}</td>
                  ${getEmail(legislator["Email"])}
                  <td data-label="Phone"><a href="tel:${legislator["Number1"]}">${legislator["Number1"]}</a></td>
                  <td data-label="Local Phone"><a href="tel:${legislator["LocalNumber"]}">${legislator["LocalNumber"]}</a></td>
                  <td data-label="City/Region">${legislator["City/Region"]}</td>
                  <td data-label="Position">${legislator["Position"]}</td>
                </tr>
              `
          })
        }
      </table>
      <div class="call-script-btn">
        <a href="#call-script">Call Script</a>
      </div>
    </div>
  `

  function getEmail(email) {
    // some emails are links to contact forms
    if(/^https/.test(email)) { 
      return html`<td data-label="Email" class="email"><a href="${email}" target="_blank">External Link</a></td>`
    } else {
      return html`<td data-label="Email" class="email"><a href="mailto:${email}">${email}</a></td>`
    }
  }

  function changeBody(e) { emit('change-body', e.target.innerHTML.toLowerCase()) }
  function isActive(body) { return state.selectedBody === body ? 'active' : '' }
}

function notFound () { return html`404` }