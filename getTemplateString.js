module.exports = function getTemplateString (url, cb) {
    fetch(url)
      .then(res => {
        res.text()
          .then(cb)
          .catch(cb(null, 'fail'))
      })
      .catch(cb('failed to fetch'))
  }
