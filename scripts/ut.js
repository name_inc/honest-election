const stringify = require('csv-stringify/lib/sync')
const fetch = require('node-fetch')

const url = 'https://le.utah.gov/data/legislators.json'

async function main () {
  const { legislators } = await (await fetch(url)).json()
  const csv = [
    ['State','Body','Name','District','Party','City/Region','Number1','LocalNumber','Email','Position'],
    ...legislators.map(it => ([
      'UT',
      { S: 'Senate', H: 'House' }[it.house],
      it.formatName,
      it.district,
      it.party,
      it.counties,
      it.workPhone || it.cell,
      it.email,
      ''
    ]))
  ]
  console.log(stringify(csv))
}

main()