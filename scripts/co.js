const scrape = require('scrape-it')
const stringify = require('csv-stringify/lib/sync')
const { sortBy } = require('lodash')

const url = 'https://leg.colorado.gov/legislators'

async function main () {
  const members = await scrapePage(url)
  const csv = [
    ['State','Body','Name','District','Party','City/Region','Number1','LocalNumber','Email','Position'],
    ...members.map(objectToRow)
  ]
  console.log(stringify(csv))
}

function objectToRow (it) {
  return [
    'CO',
    it.body,
    it.name,
    it.district,
    it.party,
    it.location,
    it.contact1,
    it.contact2,
    it.email,
    it.position
  ]
}

async function scrapePage(url) {
  const { data } = await scrape(url, {
    members: {
      listItem: '#legislators-overview-table tr',
      data: {
        name: {
          selector: 'td:nth-child(2)',
          convert: extractName
        },
        body: {
          selector: 'td:nth-child(1)',
          convert: it => ({ Representative: 'House', Senator: 'Senate' }[it])
        },
        party: {
          selector: 'td:nth-child(4)',
          convert: it => it[0]
        },
        district: {
          selector: 'td:nth-child(3)',
        },
        contact1: {
          selector: 'td:nth-child(5)',
        },
        email: {
          selector: 'td:nth-child(6)',
        }
      }
    }
  })
  return sortBy(data.members, 'body')
}

function extractName(txt) {
  const re = /([\w\s-.ñ',]+),\s+([\w\s-.ñ']+)/
  const m = re.exec(txt)
  if (m) return `${m[2]} ${m[1]}`
  return ''
}

main()