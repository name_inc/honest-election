const scrape = require('scrape-it')
const stringify = require('csv-stringify/lib/sync')

const houseUrl = 'https://www.assembly.ca.gov/assemblymembers'
const senateUrl = 'https://www.senate.ca.gov/senators'

async function main () {
  const houseMembers = await scrapeHousePage(houseUrl)
  const senateMembers = await scrapeSenatePage(senateUrl)
  const csv = [
    ['State','Body','Name','District','Party','City/Region','Number1','LocalNumber','Email','Position'],
    ...houseMembers.map(objectToRow),
    ...senateMembers.map(objectToRow)
  ]
  console.log(stringify(csv))
}

function objectToRow (it) {
  return [
    'CA',
    it.body,
    it.name,
    it.district,
    it.party,
    it.location,
    it.contact1,
    it.contact2,
    '',
    it.position
  ]
}

async function scrapeSenatePage(url) {
  function extractPhone(txt) {
    const re = /\((\d+)\)\s*(\d+)\s*-\s*(\d+)/
    const m = re.exec(txt)
    if (m) return `${m[1]}-${m[2]}-${m[3]}`
    return ''
  }

  function extractDistrict(txt) {
    const re = /District (\d+)/
    const m = re.exec(txt)
    if (m) return String(parseInt(m[1]))
    return ''
  }

  function extractName(txt) {
    const re = /([\w\s-.ñ']+)\s+\(\w\)/
    const m = re.exec(txt)
    if (m) return m[1]
    return ''
  }

  function extractParty(txt) {
    const re = /[\w\s-.ñ']+\s+\((\w)\)/
    const m = re.exec(txt)
    if (m) return m[1]
    return ''
  }

  function extractTown(txt) {
    const re = /(\w[\w\ ]+), CA/
    const m = re.exec(txt)
    if (m) return m[1]
    return ''
  }

  const { data } = await scrape(url, {
    members: {
      listItem: '.view-senator-roster > div > div',
      data: {
        name: {
          selector: 'div:nth-child(2)',
          convert: extractName
        },
        party: {
          selector: 'div:nth-child(2)',
          convert: extractParty
        },
        district: {
          selector: 'div:nth-child(3)',
          convert: extractDistrict
        },
        contact1: {
          selector: 'div:nth-child(6)',
          convert: extractPhone
        },
        contact2: {
          selector: 'div:nth-child(7)',
          convert: extractPhone
        },
        location: {
          selector: 'div:nth-child(7)',
          convert: extractTown
        }
      }
    }
  })
  data.members.forEach(it => it.body = 'Senate')
  return data.members
}

async function scrapeHousePage(url) {
  function extractPhone(txt) {
    const re = /\((\d+)\)\s*(\d+)\s*-\s*(\d+)/
    const m = re.exec(txt)
    if (m) return `${m[1]}-${m[2]}-${m[3]}`
    return ''
  }

  function extractDistrict(txt) {
    const re = /(\d+)/
    const m = re.exec(txt)
    if (m) return String(parseInt(m[1]))
    return ''
  }

  function extractPosition(txt) {
    const re = /\)\s+([\w\s]+)/m
    const m = re.exec(txt.split(/\d/)[0])
    if (m) return m[1].trim()
    return ''
  }

  function extractName(txt) {
    const re = /([\w\s-.ñ',]+),\s+([\w\s-.ñ']+)/
    const m = re.exec(txt)
    if (m) return `${m[2]} ${m[1]}`
    return ''
  }

  function extractParty(txt) {
    const re = /(\w)/
    const m = re.exec(txt)
    if (m) return m[1]
    return ''
  }

  function extractTown(txt) {
    const re = /(\w[\w\ ]+), CA/
    const m = re.exec(txt)
    if (m) return m[1]
    return ''
  }

  const { data } = await scrape(url, {
    members: {
      listItem: '.view-view-Members > div > table > tbody > tr',
      data: {
        name: {
          selector: 'td:nth-child(1)',
          convert: extractName
        },
        party: {
          selector: 'td:nth-child(3)',
          convert: extractParty
        },
        district: {
          selector: 'td:nth-child(2)',
          convert: extractDistrict
        },
        contact1: {
          selector: 'td:nth-child(4)',
          convert: extractPhone
        },
        contact2: {
          selector: 'td:nth-child(4) p',
          convert: extractPhone
        },
        location: {
          selector: 'td:nth-child(4) p',
          convert: extractTown
        }
      }
    }
  })
  data.members.forEach(it => it.body = 'House')
  return data.members
}

main()