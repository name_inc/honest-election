const scrape = require('scrape-it')
const stringify = require('csv-stringify/lib/sync')

const houseUrl = 'https://leg.wa.gov/house/representatives/Pages/default.aspx'
const senateUrl = 'https://leg.wa.gov/Senate/Senators/Pages/default.aspx'

async function main () {
  const houseMembers = await scrapePage(houseUrl)
  const senateMembers = await scrapePage(senateUrl)
  const csv = [
    ['State','Body','Name','District','Party','City/Region','Number1','LocalNumber','Email','Position'],
    ...houseMembers.map(objectToRow),
    ...senateMembers.map(objectToRow)
  ]
  console.log(stringify(csv))
}

function objectToRow (it) {
  return [
    'WA',
    it.body,
    it.name,
    it.district,
    it.party,
    it.location,
    it.contact1,
    it.contact2,
    '',
    it.position
  ]
}

async function scrapePage(url) {
  const { data } = await scrape(url, {
    members: {
      listItem: '#allMembers > div',
      data: {
        name: {
          selector: '.memberName',
          convert: extractName
        },
        body: {
          selector: '.memberName',
          convert: extractBody
        },
        party: {
          selector: '.memberName',
          convert: extractParty
        },
        position: {
          selector: '.memberFirstColumn > div:nth-child(2)',
          convert: extractPosition
        },
        district: {
          selector: '.memberFirstColumn > div:nth-child(2)',
          convert: extractDistrict
        },
        contact1: {
          selector: 'div:nth-child(2) > div',
          convert: extractPhone
        },
        contact2: {
          selector: 'div:nth-child(4) > div',
          convert: extractPhone
        },
        location: {
          selector: 'div:nth-child(4) > div',
          convert: extractTown
        }
      }
    }
  })
  return data.members
}

function extractPhone(txt) {
  const re = /\((\d+)\)\s*(\d+)\s*-\s*(\d+)/
  const m = re.exec(txt)
  if (m) return `${m[1]}-${m[2]}-${m[3]}`
  return ''
}

function extractDistrict(txt) {
  const re = /(\d+)(st|nd|rd|th)\s+Legis/
  const m = re.exec(txt)
  if (m) return String(parseInt(m[1]))
  return ''
}

function extractPosition(txt) {
  const re = /\)\s+([\w\s]+)/m
  const m = re.exec(txt.split(/\d/)[0])
  if (m) return m[1].trim()
  return ''
}

function extractBody(txt) {
  const re = /(Representative|Senator)\s*([\w\s-.ñ']+\w)\s+\(\w\)/
  const m = re.exec(txt)
  if (m) return m[1]
  return ''
}

function extractName(txt) {
  const re = /(Representative|Senator)\s*([\w\s-.ñ']+\w)\s+\(\w\)/
  const m = re.exec(txt)
  if (m) return m[2]
  return ''
}

function extractParty(txt) {
  const re = /(Representative|Senator)[\w\s-.'ñ]+\((\w)\)/
  const m = re.exec(txt)
  if (m) return m[2]
  return ''
}

function extractTown(txt) {
  const re = /(\w[\w\ ]+), WA/
  const m = re.exec(txt)
  if (m) return m[1]
  return ''
}

main()
