const stringify = require('csv-stringify/lib/sync')
const fetch = require('node-fetch')

const senateUrl = 'https://web.wyoleg.gov/LsoService/api/legislator/2020/S'
const houseUrl = 'https://web.wyoleg.gov/LsoService/api/legislator/2020/H'

async function main () {
  const senate = await (await fetch(senateUrl)).json()
  const house = await (await fetch(houseUrl)).json()
  const csv = [
    ['State','Body','Name','District','Party','City/Region','Number1','LocalNumber','Email','Position'],
    ...objsToRows('Senate', senate),
    ...objsToRows('House', house)
  ]
  console.log(stringify(csv))
}

function objsToRows (chamber, obj) {
  return obj.map(it => ([
    'WY',
    chamber,
    it.name,
    it.district,
    it.party,
    it.county,
    it.phone,
    '',
    it.eMail,
    it.leadershipPosition
  ]))
}

main()