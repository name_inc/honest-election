import pandas as pd


def email_parser(txt):
    return (txt.split(' ')[1]+'@azleg.gov')

def pos_placer(row):
    if '--' in row['Legislator']:
        pos = row['Position'] + ' ' + row['Legislator'].split(' -- ')[1]
        row['Position'] = pos
        row['Legislator'] = row['Legislator'].split(' -- ')[0]
    return row

xx=pd.read_html('AzSenate.txt')
df = pd.DataFrame(xx[0])
df['Email']=df['Email @azleg.gov'].apply(email_parser)
df['Number1'] = df['Phone']
df = df.drop(columns = ['Email @azleg.gov', 'Room', 'Phone'])
df['State'] = ['AZ']*df.shape[0]
df['Position'] =  ['Senator']*df.shape[0]
df = df.apply(pos_placer, axis = 1)
df['Body'] = ['Senate']*df.shape[0]
df['City/Region'] =  ['null']*df.shape[0]
df['LocalNumber'] = ['null']*df.shape[0]
dfHouse = df

xx = pd.read_html('AzHouse.txt')
df = pd.DataFrame(xx[0])
df['Email']=df['Email @azleg.gov'].apply(email_parser)
df['Number1'] = df['Phone']
df = df.drop(columns = ['Email @azleg.gov', 'Room', 'Phone'])
df['State'] = ['AZ']*df.shape[0]
df['Position'] =  ['House Representative']*df.shape[0]
df = df.apply(pos_placer, axis = 1)
df['Body'] = ['House']*df.shape[0]
df['City/Region'] =  ['null']*df.shape[0]
df['LocalNumber'] = ['null']*df.shape[0]

AZdf = pd.concat([df, dfHouse])
AZdf.to_csv('AZ.csv', index = False)