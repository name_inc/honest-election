const Component = require('choo/component')
const html = require('choo/html')

module.exports = class InfoView extends Component {
  constructor (name, state, emit) {
    super(name)
    this.state = state
    this.emit = emit
  }

  update () {
    // never update
    return false
  }

  createElement () {
    return html`<div id="info-overlay">
    <h1>🇺🇸 Honest Election 2020</h1>
    
    <p>NOW IS THE TIME TO CALL YOUR STATE LEGISLATURE</p>
    <h4>Demand an Honest Election in ALL STATES!</h4>
     
    <ol>
      <li>Click your State above</li>
      <li>Then look for your State representative. Start with the Senate</li>
      <li>Call and politely insist on an audit of ballots & electronic voting software in your state.</li>
      <li>Then, email them as well.</li>
      <li>Share this site with all American Patriots you know!</li>
    </ol>
    
    <div class="call-script" id="call-script">
      <h3>CALL SCRIPT:</h3>
      <p><b>Receptionist:</b>   Hello. Thank you for calling our office.</p>
      <p><b>You:</b>	Hello my name is ____. Today I’m calling to urge  [REPRESENTATIVE NAME] to support a transparent audit of votes in our state to protect all legal votes.  There have been proven irregularities nationwide, including with electronic voting systems.</p>
      <p><b>Receptionist:</b>		Great. We will make a note of this for the Representative. Is there anything else I can help you with?</p>
      <p><b>You:</b>	No, thank you. I’m only calling to ask [REPRESENTATIVE NAME] to support electoral transparency and integrity, with an urgent audit of both ballots AND electronic voting systems.</p>
      <ul>
        <li>Often, you may receive an automated message with an option to leave a message or speak directly to a staff member. If you are calling during business hours, it’s best to speak directly to a staff member.</li>
        <li>The receptionist may ask for your name, email address, or zip code. This is normal and used to track how many phone calls were received on any particular issue.</li>
        <li>DO NOT BE AFRAID to call!  And do not be afraid to call reps OUTSIDE of your state!</li>
        <li>The MOST POWERFUL way to make your voice heard is by calling & emailing your reps. Please do so ASAP, and please share.</li>
      </ul>
    </div>
     
    <ol>
      Prioritize calling people in this order:
      <li>Your local state House representative.</li>
      <li>Your local state Senator.</li>
      <li>Your federal Congressman</li>
      <li>Your federal Senator</li>
      <li>The FCC (to complain of false election reporting by CNN & FOX.) (Also file online complaint.)</li>
    </ol>
    
    <p>You can reach the FCC here: 888-225-5322, then Option 1 (for english), 4, 2, 0</p>
    <p>
      You can say: “It is against the law for tv networks to knowingly report false news. CNN, Fox, & others are reporting that Joe Biden is the, quote, “President Elect.” This is not true because a legal election winner has not been declared by official election authorities. Authorities have not made this announcement and states have not certified their votes. Please take action to prevent networks from spreading this dangerous false information, which does material harm and damage to the country. Thank you.”
    </p>
    <hr>
    <ol>
      Privacy Policy: 
      <li>We respect your privacy</li>
      <li>We DO NOT record or track your behavior, IP address, or any personally identifiable information.</li>
      <li>WE DO NOT USE COOKIES. FUK YOUR COOKIES</li>
      <li>This website is 15KB and uses MINIMAL JavaScript. And no Trackers</li>
    </ol>
    
    <ol>
      Terms of Service: <br>
      By coming to this website you agree to our terms:
      <li>ALL INFORMATION IS PUBLICLY AVAILABLE. We aggregated it in a friendly way.</li>
      <li>This information is public and accessible by all. Spread this information far and wide! </li>
      <li>We are not responsible or liable for how anyone decides to use the website or any of the publicly available information contained therein. </li>
    </ol>
  </div>

  `
  }

}

/*
    <div class="email-signup">
      <h3>THIRD PARTY (getdrip.com) EMAIL LIST:</h3>
      <style>
        #drip-ef-983308724 { font: .875rem/1.5rem sans-serif; width: 100%; }
        #drip-ef-983308724 h3 { font-size: 1.125rem; margin: 0 0 .375rem; }
        #drip-ef-983308724 > div { }
        #drip-ef-983308724 fieldset { border: none; margin: 0; padding: 0; }
        #drip-ef-983308724 legend { margin: 0; padding: 0; }
        #drip-ef-983308724 input[type="email"],
        #drip-ef-983308724 input[type="number"],
        #drip-ef-983308724 input[type="tel"],
        #drip-ef-983308724 input[type="text"] { margin: 0; padding: .375rem .5625rem; width: 100%; }
        #drip-ef-983308724 input[type="checkbox"],
        #drip-ef-983308724 input[type="radio"] { margin: .1875rem .1875rem 0 0; padding: 0; }
        #drip-ef-983308724 input[type="submit"] { margin: 0; padding: .375rem .5625rem; }
      </style>
<form action="https://getdrip.com/forms/983308724/submissions…" method="post" data-drip-embedded-form="983308724" id="drip-ef-983308724">
<div data-drip-attribute="description"><strong>WANT THE LATEST ELECTION FRAUD UPDATES?</strong>
  <br>
Sign Up to receive one simple 'Daily Digest' email with <strong>UNFILTERED</strong> election fraud updates each morning from those with&nbsp;<strong>CUTTING EDGE </strong>information.</div>

<div>
    <label for="drip-email">Email Address</label><br>
    <input type="email" id="drip-email" name="fields[email]" value="">
</div>

<div>
    <label for="drip-phone">Text Updates (OPTIONAL)</label><br>
    <input type="tel" id="drip-phone" name="fields[phone]" value="" placeholder="+1 201 555-1234">
      <div>
        <label class="sms-subtext" data-role="sms-consent-message">
          <!-- This text must not be modified -->
         By submitting your mobile number on this form, you agree to receive
promotional and personalized messages at the phone number provided. Reply STOP to
unsubscribe or HELP for help. Message and data rates may apply. View our
<a target="_blank" href="https://HonestElection2020.com">Terms of Service</a>
and
<a target="_blank" href="https://HonestElection2020.com">Privacy Policy</a>
for more information.

        </label>
      </div>
</div>

<div style="display: none;" aria-hidden="true">
  <label for="website">Website</label><br>
  <input type="text" id="website" name="website" tabindex="-1" autocomplete="false" value="">
</div>
<input type="hidden" name="tags[]" id="tags_" value="Main Page Optin" tabindex="-1">

<div>
  <input type="submit" value="Sign Up" data-drip-attribute="sign-up-button">
</div>
</form>
    </div>
    
*/