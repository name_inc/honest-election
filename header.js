const Component = require('choo/component')
const html = require('choo/html')

module.exports = class Header extends Component {
  constructor (name, state, emit) {
    super(name)
    this.state = state
    this.emit = emit
  }

  update () {
    return false
  }

  createElement () {
    return html`
      <header class="header">
        <h1>todos</h1>
      </header>
    `
  }

}