Get State Senate and House
CSV data format for standardized data format to create website:


State,Body,Name,District,Party,City/Region,Number1,LocalNumber,Email,Position


| State | @ | Pastebin |
| ------ | ------ | ------ |
| PA | @satisfypolitics | https://pastebin.com/av9WL9Yh |
| AZ | @puhcko	 | https://pastebin.com/xd1vnbSu |
| MI | @CrestonScheel | https://pastebin.com/kSm10E8q |
| GA | @satisfypolitics | https://pastebin.com/8gBUT2v6 |
| NV | @hrafnhraf | https://pastebin.com/nPGt2qPX |
| WI | @maxwarez | https://pastebin.com/1JrgPZBz |
| VA | @riots_are_bad | https://pastebin.com/TzA7M9b2
| WA | @hrafnhraf | https://pastebin.com/t4XGL7p2 |
| CA | @hrafnhraf | https://pastebin.com/TjRxhHvQ |
| UT | @hrafnhraf | https://pastebin.com/qM48PJNT |
| WY | @hrafnhraf | https://pastebin.com/9M5X6kLJ |
| CO | @hrafnhraf | https://pastebin.com/pvhv79uu |
| MN | @satisfypolitics | https://pastebin.com/sziacMZX


http-server --proxy http://localhost:8080? .